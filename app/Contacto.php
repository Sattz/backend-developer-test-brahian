<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacto extends Model
{
    //
    #protected $table='contactos';
    public $timestamps = true;
    #protected $fillable = ['created_at', 'updated_at']; 

}
