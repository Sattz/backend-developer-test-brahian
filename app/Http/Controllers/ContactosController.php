<?php

namespace App\Http\Controllers;
use App\Contacto;
use Illuminate\Http\Request;
use DB;
class ContactosController extends Controller
{
    /**
     * Se encarga de registrar la informacion basica
     * del api hubapi en la db
     * @return $message <Json>
     */
    public function crearContactos()
    {   
        $message = ["exito" => true, "mensaje" => "Finaliza la importacion"];

        $url = "https://api.hubapi.com/contacts/v1/lists/all/contacts/all?hapikey=8997b045-f7a9-49b4-8353-b0c798c4ff94";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $message["exito"] = false;
            $message["mensaje"] = "Error al intentar obtener datos del api externa";        
        } else {
            $to_array = json_decode($response, true);
            
            //leemos informacion retornada del api y procedemos a obtener los datos necesarios para ser ingresados a la db
            
            $contactos = $to_array["contacts"];
            $insertar = [];

            foreach($contactos as $contacto){
                $tmp_identity = $contacto["identity-profiles"];

                foreach($tmp_identity as $identity){
                    $tmp_identities = $identity["identities"];
                    $tmp_email = $this->obtenrEmail($tmp_identities);
                }

                $tmp_properties = $contacto["properties"];
                //debido a que insert es un metodo de query builder no de eloquent model las fechas automaticas no son seteadas ...
                $tmp_date = date("Y-m-d H:i:s");
                
                $tmp_data_a_insertar = [
                    "external_id" => $contacto["vid"],
                    "first_name" => $tmp_properties["firstname"]["value"] ?? NULL,
                    "last_name" => $tmp_properties["lastname"]["value"] ?? NULL,
                    "email" => $tmp_email,
                    "external_created_at" => date('Y-m-d H:i:s', ($contacto["addedAt"] / 1000)),
                    "created_at" => $tmp_date,
                    "updated_at" => $tmp_date
                ];

                $insertar[] = $tmp_data_a_insertar;
            }
            
            Contacto::insert($insertar);

            //en caso de ser muchas informacion a insertar se podria utilizar un metodo similar a:
            
            #$insertar = collect($insertar);
            
            #$delimitador = $insertar->chunk(250);
            #foreach($delimitador as $fragmento){
            #    $fragmento_array = $fragmento->toArray();
            #    Contacto::insert($fragmento_array);
            #}
        }
        return response()->json($message);
    }

    /**
     * Analisa y encuentra el correo mas reciente
     *
     * @param array $emails
     * @return $email
     */
    public function obtenrEmail($emails=[])
    {
        $tmp_identities = $emails;
        $tmp_date = 0;
        #$i = -1;
        $email = "";
        foreach($tmp_identities as $indice => $entidad){
            $tipo = $entidad["type"];
            $tmp_timestamp = (int)$entidad["timestamp"];
            if($tipo === "EMAIL"){
                if($tmp_date <= $tmp_timestamp){
                    $tmp_date = $tmp_timestamp;
                    #$i = $indice;
                    $email = $entidad["value"];
                }  
            }    
        }
        return $email;
    }

    /**
     * Retorna lista de contactos, pueden ser filtrados por el correo
     *
     * @return void
     */
    public function listarContactos(Request $request)
    {
        $data = $request->all();
        $sql = "SELECT * FROM contactos ";
        $contactos = [];
        $valores_sql = [];
        $where = " WHERE ";
        $tiene_filtros = false;
        if(isset($data["email"])){
            $where .= " email  = :mail ";
            $valores_sql["mail"] = $data["email"];
            $tiene_filtros = true;
        }

        if($tiene_filtros){
            $sql.= $where;
        }
        #dd($valores_sql);
        $contactos = DB::select($sql, $valores_sql);
        #$contactos = Contacto::get();

        return response()->json($contactos);
    }
}
