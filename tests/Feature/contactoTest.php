<?php

namespace Tests\Feature;

use App\Contacto;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class contactoTest extends TestCase
{
    use RefreshDatabase;
    
    public function test_contacto_has_data()
    {   
        $this->withoutExceptionHandling();
        
        $data = Factory(Contacto::class, 10)->create();
        
        $response = $this->get('/listar/conctactos');

        $response->assertStatus(200)
            ->assertJsonCount(10)
            ->assertJson($data->toArray());
    }

    public function test_contacto_can_be_filtered_by_email()
    {
        $this->withoutExceptionHandling();
        
        $data = Factory(Contacto::class, 1)->create();
       
        $response = $this->get('/listar/conctactos?email='.$data[0]->email);
        #dd($data->toArray(),$response );
        $response->assertStatus(200)
            ->assertJson($data->toArray());
    }
}
