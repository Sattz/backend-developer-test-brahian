<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Contacto;
use Faker\Generator as Faker;

$factory->define(Contacto::class, function (Faker $faker) {
    return [
        "external_id" => $faker->randomNumber(),
        "first_name" => $faker->firstName(),
        "last_name" => $faker->lastName(),
        "email" => $faker->email(),
        "external_created_at" => $faker->date()
    ];
});
